import urllib3
import json
import os
from bs4 import BeautifulSoup
from errata_tool import Erratum
from errata_tool.release import Release


urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")

if 'REQUESTS_CA_BUNDLE' not in os.environ:

    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/pki/tls/certs/ca-bundle.crt'

release = '8.4.0.Z.MAIN+EUS'
data_kernel = Release(name='RHEL-8.4.0.Z.MAIN+EUS')
advisories = data_kernel.advisories()

prep_ship = [a for a in advisories if a['synopsis'] == 'Important: kernel-rt security and bug fix update']
json_manipulate = json.dumps(prep_ship, indent=2, sort_keys=True)
kernel_string = json_manipulate[1:len(json_manipulate)-1]


ship_live = [a for a in advisories if a['synopsis'] == 'Moderate: kernel-rt security and bug fix update']
positive_manipulate = json.dumps(ship_live, indent=2, sort_keys=True)

neutral_string = positive_manipulate[1:len(positive_manipulate)-1]

batch4 = prep_ship[4]['id']
e = Erratum(product='RHEL', errata_id=batch4)
e_nvr = e.errata_builds["RHEL-8.4.0.Z.MAIN+EUS"][0]
e_brew = e.url()
e_name= e.errata_name
e_date = e.publish_date

print("", prep_ship[4]['release'], 1*"\n",  prep_ship[4]['id'], prep_ship[4]['status'], e_nvr, e_brew, e_date)



batch2 = prep_ship[3]['id']
z = Erratum(product='RHEL', errata_id=batch2)
f_version = z.errata_builds["RHEL-8.4.0.Z.MAIN+EUS"][0]
f_date = z.publish_date
z_url = z.url()
f_rhsa = z.errata_name

print("", "\n", prep_ship[3]['id'], prep_ship[3]['status'], f_version, z_url, f_date)

batch1 = prep_ship[1]['id']
h = Erratum(product='RHEL', errata_id=batch1)
h_url = h.url()
h_rhsa = h.errata_name
h_build = h.errata_builds["RHEL-8.4.0.Z.MAIN+EUS"][0]
h_pubdate = h.publish_date


print("", 1*"\n",  prep_ship[1]['id'], prep_ship[1]['status'], h_build, h_url, h_pubdate)





async1 = prep_ship[2]['id']
g = Erratum(product='RHEL', errata_id=async1)
ratta_link = g.url()
g_rhsa = g.errata_name
g_build = g.errata_builds["RHEL-8.4.0.Z.MAIN+EUS"][0]
g_pubdate = g.publish_date



print("", 1*"\n",  prep_ship[2]['id'], prep_ship[2]['status'], g_build, ratta_link, g_pubdate)




async2 = prep_ship[0]['id']
i = Erratum(product='RHEL', errata_id=async2)
i_url = i.url()
i_rhsa = i.errata_name
i_build = i.errata_builds["RHEL-8.4.0.Z.MAIN+EUS"][0]
i_pubdate = i.publish_date

print("", 1*"\n",  prep_ship[0]['id'], prep_ship[0]['status'], i_build, i_url, i_pubdate)


async3 = ship_live[0]['id']
j = Erratum(product='RHEL', errata_id=async3)
j_url = j.url()
j_rhsa = j.errata_name
j_build = j.errata_builds["RHEL-8.4.0.Z.MAIN+EUS"][0]
j_pubdate = j.publish_date

print("", 1*"\n",  ship_live[0]['id'], ship_live[0]['status'], j_build, j_url, j_pubdate)


async4 = ship_live[1]['id']
k = Erratum(product='RHEL', errata_id=async4)
k_url = k.url()
k_rhsa = k.errata_name
k_build = k.errata_builds["RHEL-8.4.0.Z.MAIN+EUS"][0]
k_pubdate = k.publish_date

print("", 1*"\n",  ship_live[1]['id'], ship_live[1]['status'], k_build, k_url, k_pubdate)


# 'RHEL-8.4.0.GA'

data_kernel = Release(name='RHEL-8.4.0.GA')
advisories = data_kernel.advisories()
preppy_ship = [a for a in advisories if a['synopsis'] == 'Important: kernel-rt security and bug fix update']

batch10 = preppy_ship[0]['id']
m = Erratum(product='RHEL', errata_id=batch10)
m_url = m.url()
m_rhsa = m.errata_name
m_build = m.errata_builds["RHEL-8.4.0.GA"][0]
m_pubdate = m.publish_date

