#!/usr/bin/python3.9
import os
import json.decoder
import json
import re
from errata_tool import Erratum
import requests
from requests_gssapi import HTTPSPNEGOAuth
import urllib3


_auth = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")


if 'REQUESTS_CA_BUNDLE' not in os.environ:

    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/pki/tls/certs/ca-bundle.crt'

def errata():

    nutmeg = requests.get(url='https://errata.devel.redhat.com/release/RHEL_8_2_0_Z_AUS/advisories.json', auth=HTTPSPNEGOAuth(_auth))

    new_dumps = (json.dumps(json.loads(nutmeg.text), sort_keys=True, indent=4, separators=(",", ": ")))

    with open('helper.json', 'w') as e:
        e.write(new_dumps)

    with open("helper.json", 'r') as e:
        json_object = json.loads(e.read())

    for z in range(0, (len(json_object))):
        b = json_object[z]
        brew_link = 'https://brewweb.engineering.redhat.com/brew/search?match=glob&type=build&terms=+'
        if re.findall(r"[a-zA-Z]: kernel [a-zA-Z]", str(b)):
            x = Erratum(product='RHEL', errata_id=b['id'])
            jump_man = x.errata_id, x.errata_builds["RHEL-8.2.0.Z.TUS"][0], x.errata_state, x.ship_date, x.errata_name, x.url()
            brewing_it = brew_link + str(jump_man[1])

            print(x)

    westbrook = requests.get(url='https://errata.devel.redhat.com/release/RHEL_8_2_0_Z_AUS/advisories.json', auth=HTTPSPNEGOAuth(_auth))

    westbrook_dumps = (json.dumps(json.loads(westbrook.text), sort_keys=True, indent=4, separators=(",", ": ")))

    with open('helper.json', 'w') as e:
        e.write(westbrook_dumps)

    with open("helper.json", 'r') as e:
        our_money = json.loads(e.read())

    for z in range(0, (len(our_money))):
        b = our_money[z]
        brew_link = 'https://brewweb.engineering.redhat.com/brew/search?match=glob&type=build&terms=+'
        if re.findall(r"[a-zA-Z]: kernel-rt [a-zA-Z]", str(b)):
            x = Erratum(product='RHEL', errata_id=b['id'])
            jump_man = x.errata_id, x.errata_builds["RHEL-8.2.0.Z.TUS"][0], x.errata_state, x.ship_date, x.errata_name, x.url()
            brewing_it = brew_link + str(jump_man[1])

            print(x)

    jordan = requests.get(url='https://errata.devel.redhat.com/release/RHEL_8_7_0_Z_MAIN/advisories.json', auth=HTTPSPNEGOAuth(_auth))

    jordan_dumps = (json.dumps(json.loads(jordan.text), sort_keys=True, indent=4, separators=(",", ": ")))

    with open('helper.json', 'w') as e:
        e.write(jordan_dumps)

    with open("helper.json", 'r') as e:
        the_money = json.loads(e.read())

    for z in range(0, (len(the_money))):
        b = the_money[z]
        brew_link = 'https://brewweb.engineering.redhat.com/brew/search?match=glob&type=build&terms=+'
        if re.findall(r"[a-zA-Z]: kernel [a-zA-Z]", str(b)):
            x = Erratum(product='RHEL', errata_id=b['id'])
            jump_man = x.errata_id, x.errata_builds["RHEL-8.7.0.Z.MAIN"][0], x.errata_state, x.ship_date, x.errata_name, x.url()
            brewing_it = brew_link + str(jump_man[1])

            print(x)



    durant = requests.get(url='https://errata.devel.redhat.com/release/RHEL_8_7_0_Z_MAIN/advisories.json', auth=HTTPSPNEGOAuth(_auth))

    durant_dumps = (json.dumps(json.loads(durant.text), sort_keys=True, indent=4, separators=(",", ": ")))

    with open('helper.json', 'w') as e:
        e.write(durant_dumps)

    with open("helper.json", 'r') as e:
        family_money = json.loads(e.read())

    for z in range(0, (len(family_money))):
        b = family_money[z]
        brew_link = 'https://brewweb.engineering.redhat.com/brew/search?match=glob&type=build&terms=+'
        if re.findall(r"[a-zA-Z]: kernel-rt [a-zA-Z]", str(b)):
            x = Erratum(product='RHEL', errata_id=b['id'])
            jump_man = x.errata_id, x.errata_builds["RHEL-8.7.0.Z.MAIN"][0], x.errata_state, x.ship_date, x.errata_name, x.url()
            brewing_it = brew_link + str(jump_man[1])

            print(x, brewing_it)


if __name__ == '__main__':
    errata()

