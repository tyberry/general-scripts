#!/usr/bin/python3.9
import urllib3
import json
import os
from errata_tool import Erratum
from errata_tool.release import Release


urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")

if 'REQUESTS_CA_BUNDLE' not in os.environ:

    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/pki/tls/certs/ca-bundle.crt'

def rata_rata(prep_ship):

    data_kernel = Release(name=prep_ship)
    advisories = data_kernel.advisories()


    return [a for a in advisories if a['synopsis'] == 'Important kernel-rt security and bug fix update'], \



release = ['RHEL-8.2.0.Z.EUS']

index = 0

try:
    while index < len(release):
        release[index] = rata_rata(release[index])
        index = index + 1
except IndexError:
    pass

j_string = json.dumps(release, indent=1)
# print(j_string)

with open('errata-finder.json', 'w') as f:
    f.write(j_string)

with open('errata-finder.json', 'r') as e:
    json_string = json.loads(e.read())
try:
    for z in range(0, 15):
        e = Erratum(product='RHEL', errata_id=json_string[0][0][z]['id'])
        jump_man = e.errata_id, e.errata_builds["RHEL-8.2.0.Z.EUS"][0], e.errata_state, e.url()
        print(jump_man)
except IndexError:
    pass
