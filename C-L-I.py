#!/usr/bin/python3.9
import json.decoder
import urllib3
import requests
import json
from API_Token import PAT, brew_link
from bs4 import BeautifulSoup
import argparse
import subprocess
import re
from NSHS import DATE, RHSA, k_versions, e_link

urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")


def commands():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-p", "--prettify", help="prettify json data with bs4", action="store_true")
    group.add_argument("-e", "--errata", help="subprocess for the ship function in errata.py", action="store_true")
    group.add_argument("-a", "--append", help="Builds a brew url for the PUT call", action="store_true")
    group.add_argument("-u", "--update", help="executes PUT call to confluence page", action="store_true")
    args = parser.parse_args()

    if args.prettify:

        auth_token = PAT
        headers = {'Authorization': 'Bearer ' + auth_token}

        url = 'https://docs.engineering.redhat.com/rest/api/content/297259803?expand=body.view,version'

        payload = json.dumps({

            "_expandable": {
                "container": "",
                "metadata": "",
                "operations": "",
                "children": "",
                "restrictions": "/",
                "ancestors": "",
                "body": "",
                "descendants": ""
            },
            "_links": {
                "webui": "",
                "edit": "",
                "tinyui": "",
                "collection": "",
                "base": "",
                "context": "",
                "self": ""
            },

        })

        response = requests.get(url, data=payload, headers=headers, verify="/etc/ssl/certs/ca-bundle.trust.crt")

        results = json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": "))

        f = open('RHEL8-RT-Release-Page-Testing_2.json', 'w')

        f.write(str(results))

        with open('RHEL8-RT-Release-History.json', 'r') as html_file:
            soup = BeautifulSoup(html_file, 'lxml')
            result = soup.prettify()
            latestrelease = soup.a.text

        print(str(result +"\nMost recent kernel-rt release found on the confluence table: "+latestrelease))

    if args.errata:
         # print(kernel_string)
        # # print(string_kernel)
        # # print(sunny_day)
        #
        # with open('kernel-streams(8.4 EUS).json', 'w') as k:
        #     object_json = json.dumps(kernel_string)
        #     k.write(object_json)
        #     # print(object_json[0])
        #

            # g = object_json['id']
        # print(object_json)





        # k = open('kernel-streams(8.4 EUS).json')
        # k.write(str(kernel_string))
        # subprocess.run(['./errata.py -r 8.4 kernel kernel-rt'], shell=True)
        #
        # subprocess.run(['./errata.py -s 8.4 kernel kernel-rt'], shell=True)
        #
        subprocess.run(['./errata.py -s 8.2 kernel kernel-rt > errata-headers.txt'], shell=True)


    if args.append:
        # add regex for Date, kernel build and RHSCA
        regex = open('errata-headers.txt', 'r')
        nvr = regex.read()
        new_list = nvr

        errata_headers = (re.split(r'\n', str(new_list)))

        # print(errata_headers[7])

        kernel_rt = errata_headers[7]

        matches = re.finditer(r"kernel\Drt\D\d\.(\d{2,4}\.\d\D\d{1,4})(\.\d{1,3}\.\d)(\.\w{1,3}\d{1,3})(\.\d{1,4})(\.\w{1,3}\D\d)", kernel_rt)
        #
        smatches = re.finditer(r"RHSA-2022:1209-02", kernel_rt)

        catches = re.finditer(r"2022-04-05 ", kernel_rt)

        for matchNum, match in enumerate(matches, start=1):
            patch = match.group(0)
            new_brew = str(brew_link) + str(k_versions)

        for matchNum, match in enumerate(smatches, start=1):
            rhsa = match.group(0)
            # print(rhsa)

        for matchNum, match in enumerate(catches, start=1):
            date = match.group(0)
            # print(date)

        with open('kernel-rt-table-copy.json', 'r') as e:
            json_object = json.loads(e.read())

        b = json_object['body']['view']['value']
        cool_object = b
        # json_string = json.dumps(cool_object, indent=4)

        with open('swap-file-copy.html', 'w') as f:
            f.write(b)
            f.close()

        with open('swap-file.html', 'w') as lion:
            lion.write(b)
            lion.close()

        with open('swap-file.html', 'r') as html_file:
            soup = BeautifulSoup(html_file, 'lxml')
            tag = soup.find_all('table')

        jade = open('swap-file.html', 'w')
        jade.write(str(tag[0]))
        jade.close()

        with open('swap-file.html', 'r') as omni:
            html_read = omni.read()
        v = html_read
        # print(v)


        with open('swap-file-copy.html', 'w') as x:
            new_json = re.sub('8.3.0', '8.4.0', str(cool_object))
            swapness = re.sub(r"kernel\Drt\D\d\.(\d{2,4}\.\d\D\d{1,4})(\.\d{1,3}\.\d)(\.\w{1,3}\d{1,3})(\.\d{1,4})(\.\w{1,3}\D\d)", k_versions, str(new_json), count=1)
            swap3 = re.sub(r"https:\W{1,4}\w{1,8}\W\w{1,13}\W\w{1,8}\W\w{1,8}\W\w{1,8}\W\w{1,10}\W\w{1,7}\W\w{1,7}", new_brew, str(swapness), count=1)
            swap4 = re.sub(r"https://errata.devel.redhat.com/advisory/69346", e_link, str(swap3), count=1)
            swap5 = re.sub("RHSA-2021:1081", RHSA, str(swap4), count=1)
            swap6 = re.sub("2021-04-06", DATE, str(swap5), count=1)


            # print(swap4)


            #
            x.write(swap6)
            x.close()

        with open('swap-file-copy.html', 'r') as html_file:
            soup = BeautifulSoup(html_file, 'lxml')
            table_wrap = soup.new_tag("table", style="width:35.4001%")
            batman = soup.find_all('p', limit=1)
            batman_new = batman

            # print(batman_new)

            # table = soup.find_all('table')
            kstuff = soup.find_all('tr')
            new_array = kstuff
            krt = new_array.copy()

            table_headers = kstuff[0]
            release_header = kstuff[1]
            first_row = new_array[2]
            second_row = new_array[3]
            third_row = new_array[4]

            # new_array = sound.copy()
            # dup_row = str(new_array[2])
            # print(table_headers)

            # print(first_row, second_row, third_row)
            # print(batman_new[0])

            # kstuff = soup.find_all('tr')
            # new_array = kstuff
            # krt = new_array.copy()
            # # deeds = str(new_array[2])
            # new_array[2] = swap1
            # new_array[7] = krt[2]
            # new_array[12] = krt[7]





            table_wrap.insert(0, table_headers)
            table_wrap.insert(1, release_header)
            table_wrap.insert(2, first_row)
            # table_wrap.insert(4, second_row)
            # table_wrap.insert(5, third_row)

            # print(table_wrap)
            # new_array[2] = swap1
            # new_array[7] = krt[2]
            # new_array[12] = krt[7]

        batman_new.insert(1, table_wrap)
        batman_new.insert(2, release_header)
        batman_new.insert(3, first_row)
        # batman_new.insert(4, second_row)
        # batman_new.insert(5, third_row)

        # reassign array values and swap values out https://www.geeksforgeeks.org/change-the-tags-contents-and-replace-with-the-given-string-using-beautifulsoup/

        q = open('swap-file-copy.html', 'w')
        q.write(str(batman_new[0]) + str(batman_new[1]))
        q.close()

        with open('swap-file-copy.html', 'r') as o:
            my_html = o.read()
        s = my_html
        print(s)
        #
        auth_token = PAT

        headers = {'Authorization': 'Bearer ' + auth_token,
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'
                   }

        # # # # line below is an adjustable parameter to swap in for "s" give it a try if you'd like :) try line 409.
        # # # # c = "<li> Hi I'm bob </li> <li> Hi I'm leonard </li> <li> Hi I'm angie </li> <li> Hi I'm Ty </li>         #
        # with open('RHEL8-RT-Release-History-TEST-PAGE.json', 'r') as e:
        #     json_object = json.loads(e.read())
        # #
        # num = json_object['version']['number']

        json_string = {
            "version": {
                "number": 126
            },
            "title": "Test Release History Page",
            "type": "page",
            "space": {"key": "KRATS"},
            "body": {
                "storage": {
                    "value": s,
                    "representation": "storage"
                }
            }
                        }

        website = 'https://docs.engineering.redhat.com/rest/api/content/249904692?expand=body.view,space'

        post = requests.put(url=website, data=json.dumps(json_string), headers=headers, verify="/etc/ssl/certs/ca-bundle.trust.crt")

        print(post.status_code)
        #
        new_dumps = (json.dumps(json.loads(post.text), sort_keys=True, indent=4, separators=(",", ": ")))
        #
        print(new_dumps)

        z = open('RHEL8-RT-Release-History-TEST-PAGE.json', 'w')

        z.write(str(new_dumps))


if __name__ == '__main__':
    commands()
