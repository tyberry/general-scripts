#!/usr/bin/python3.9
import urllib3
import json
import os
from errata_tool.release import Release


urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")

if 'REQUESTS_CA_BUNDLE' not in os.environ:

    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/pki/tls/certs/ca-bundle.crt'

def errata(prep_ship):

    data_kernel = Release(name=prep_ship)
    advisories = data_kernel.advisories()


    return [a for a in advisories if a['synopsis'] == 'Important: kernel-rt security and bug fix update'],\
           [a for a in advisories if a['synopsis'] == 'Important: kernel-rt security update'], \
           [a for a in advisories if a['synopsis'] == 'kernel-rt bug fix update'], \
           [a for a in advisories if a['synopsis'] == 'Moderate: kernel-rt security and bug fix update'], \
           [a for a in advisories if a['synopsis'] == 'Critical: kernel-rt security and bug fix update'],\
           [a for a in advisories if a['synopsis'] == 'Low: kernel-rt security and bug fix update'], \







release = ['RHEL-8.0.0.Z', 'RHEL-8.1.0', 'RHEL-8.1.0.Z.MAIN+EUS', 'RHEL-8.2.0.GA', 'RHEL-8.2.0.Z.MAIN+EUS', 'RHEL-8.2.0.Z.EUS', 'RHEL-8.3.0.GA', 'RHEL-8.3.0.Z.MAIN',
           'RHEL-8.4.0.GA', 'RHEL-8.4.0.Z.MAIN+EUS', 'RHEL-8.4.0.Z.EUS', 'RHEL-8.5.0.GA', 'RHEL-8.5.0.Z.MAIN', 'RHEL-8.6.0.GA', 'RHEL-8.6.0.Z.MAIN+EUS']

index = 0


try:
    while index < len(release):
        release[index] = errata(release[index])
        index = index + 1
except IndexError:
    pass

j_string = json.dumps(release, indent=1)
print(j_string)

with open('newmoney.json', 'w') as f:
    f.write(j_string)





