#!/usr/bin/python3.9
import urllib3
import json
import os
from errata_tool import Erratum
from errata_tool.release import Release

urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")

if 'REQUESTS_CA_BUNDLE' not in os.environ:

    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/pki/tls/certs/ca-bundle.crt'


with open('newmoney.json', 'r') as e:
    json_object = json.loads(e.read())

print("Finding all RHEL Releases \n")


print(Erratum(product='RHEL', errata_id=json_object[0][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[0][0][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[0][0][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[0][1][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[0][1][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[0][1][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[1][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[2][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[2][1][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[2][2][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[2][3][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[2][1][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[4][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[4][0][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[4][0][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[4][0][3]['id']))
print(Erratum(product='RHEL', errata_id=json_object[4][2][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[4][3][0]['id']))

print(Erratum(product='RHEL', errata_id=json_object[5][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][3]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][4]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][5]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][6]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][7]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][8]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][9]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][0][10]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][1][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][1][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][1][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[5][1][3]['id']))
print(Erratum(product='RHEL', errata_id=json_object[6][1][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[7][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[7][0][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[7][0][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[8][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[9][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[9][0][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[9][0][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[9][0][3]['id']))
print(Erratum(product='RHEL', errata_id=json_object[9][0][4]['id']))
print(Erratum(product='RHEL', errata_id=json_object[9][1][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[9][1][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[10][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[10][0][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[10][0][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[10][0][3]['id']))
print(Erratum(product='RHEL', errata_id=json_object[10][0][4]['id']))
print(Erratum(product='RHEL', errata_id=json_object[10][0][5]['id']))
print(Erratum(product='RHEL', errata_id=json_object[11][1][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[12][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[12][0][1]['id']))
print(Erratum(product='RHEL', errata_id=json_object[12][0][2]['id']))
print(Erratum(product='RHEL', errata_id=json_object[12][0][3]['id']))
print(Erratum(product='RHEL', errata_id=json_object[12][1][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[13][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[14][0][0]['id']))
print(Erratum(product='RHEL', errata_id=json_object[14][0][1]['id']))








