#!/usr/bin/python3.9
import json.decoder
import urllib3
import requests
import json
from API_Token import PAT, brew_link
from bs4 import BeautifulSoup
import re
from errata_tool import Erratum

urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")


def command():
        auth_token = PAT

        headers = {'Authorization': 'Bearer ' + auth_token,
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'}

        json_string = {
            "version": {
                "number": +1
            },
            "title": "Test Release History Page 1",
            "type": "page",
            "space": {"key": "KRATS"},
            "body": {
                "storage": {
                    "value": +1,
                    "representation": "storage"
                }
            }
        }
        website = 'https://docs.engineering.redhat.com/rest/api/content/304821200?expand=body.view,version'

        get = requests.get(url=website, data=json.dumps(json_string), headers=headers,
                           verify="/etc/ssl/certs/ca-bundle.trust.crt")

        new_dumps = (json.dumps(json.loads(get.text), sort_keys=True, indent=4, separators=(",", ": ")))

        print(new_dumps)

        z = open('RHEL8-RT-Release-History-Page-Testing_1.json', 'w')

        z.write(str(new_dumps))

        z.close()


        with open('newmoney.json', 'r') as e:
            json_string = json.loads(e.read())

        with open("RHEL8-RT-Release-History-Page-Testing_1.json", 'r') as e:
            json_object = json.loads(e.read())

        b = json_object['body']['view']['value']
        cool_object = b
        release_version = "RHEL-8.1.0.Z"
        # mile_= '8.0.0.z'
        # stone_ = 'async'

        with open('swap-table-2.html', 'w') as lion:
            lion.write(b)
            # print(b)
            lion.close()

        with open('swap-table-2.html', 'w') as x:
            old_object = cool_object.split()

        for z in range(0, 1):
            e = Erratum(product='RHEL', errata_id=json_string[1][0][z]['id'])
            jump_man = e.errata_id, e.errata_builds["RHEL-8.1.0"][0], e.errata_state, e.ship_date, e.errata_name, e.url()
            # change index for table and 8.4.0.z batch to async
            if z == 0:
                brews_bros = brew_link + str(jump_man[1])
                build_venom = str(jump_man[1])
                date_venom = str(jump_man[3])
                rhsa_venom = str(jump_man[4])
                url_venom = str(jump_man[5])

                # First row

                release_header = re.sub(r"RHEL-8.0.0.Z", release_version, str(old_object[16]))
                old_object[16] = release_header

                # brew href
                updated_build = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", str(brews_bros), str(old_object[23]))
                old_object[23] = updated_build

                # # nvr
                _nvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d",str(build_venom), str(old_object[24]))
                old_object[24] = _nvr

                # # # date
                _date = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", str(date_venom), str(old_object[26]))
                old_object[26] = _date

                # advisory link
                _adlink = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", str(url_venom), str(old_object[30]))
                old_object[30] = _adlink

                # RHSA
                _rsha = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", str(rhsa_venom), str(old_object[31]))
                old_object[31] = _rsha
                continue

        for z in range(0, 4):
            e = Erratum(product='RHEL', errata_id=json_string[2][z][0]['id'])
            jump_man = e.errata_id, e.errata_builds["RHEL-8.1.0.Z.MAIN+EUS"][0], e.errata_state, e.ship_date, e.errata_name, e.url()

            if z == 0:
                brews_bros = brew_link + str(jump_man[1])
                build_venom = str(jump_man[1])
                date_venom = str(jump_man[3])
                rhsa_venom = str(jump_man[4])
                url_venom = str(jump_man[5])

                # Second row

                # brew url
                updated_brew = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", str(brews_bros), str(old_object[39]))
                old_object[39] = updated_brew

                # # # nvr
                updated_row = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d",str(build_venom), str(old_object[40]))
                old_object[40] = updated_row

                #  date
                update_date = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", str(date_venom), str(old_object[42]))
                old_object[42] = update_date

                # advisory link
                new_adlink = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", str(url_venom), str(old_object[46]))
                old_object[46] = new_adlink

                # RHSA
                new_rsha = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", str(rhsa_venom), str(old_object[47]))
                old_object[47] = new_rsha
                continue

            if z == 1:
                brews_bros = brew_link + str(jump_man[1])
                build_venom = str(jump_man[1])
                date_venom = str(jump_man[3])
                rhsa_venom = str(jump_man[4])
                url_venom = str(jump_man[5])

            # third row
                # brew href
                updated_href = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brews_bros, str(old_object[55]))
                old_object[55] = updated_href

                # nvr
                updated_nvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", build_venom, str(old_object[56]))
                old_object[56] = updated_nvr

                # date
                update_shipdate = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", date_venom, str(old_object[58]))
                old_object[58] = update_shipdate

                # advisory link
                ad_link = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", url_venom, str(old_object[62]))
                old_object[62] = ad_link

                # RHSA
                next_rhsa = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", rhsa_venom, str(old_object[63]))
                old_object[63] = next_rhsa
                continue


            if z == 2:
                brews_bros = brew_link + str(jump_man[1])
                build_venom = str(jump_man[1])
                date_venom = str(jump_man[3])
                rhsa_venom = str(jump_man[4])
                url_venom = str(jump_man[5])

            # Fourth row
            #     grey_stone = re.sub(r'8.4.0.z', mile_, str(old_object[67]))
            #     old_object[67] = grey_stone
            #
            #     black_stone = re.sub(r'batch#\d',stone_, str(old_object[68]))
            #     old_object[68] = black_stone

                # brew href
                b_link = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brews_bros, str(old_object[72]))
                old_object[72] = b_link

                # nvr
                row_fifth = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", build_venom, str(old_object[73]))
                old_object[73] = row_fifth

                # date
                date_fifth = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", date_venom, str(old_object[77]))
                old_object[77] = date_fifth

                #  advisory link
                adlink_errata = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", url_venom, str(old_object[81]))
                old_object[81] = adlink_errata

                # RHSA
                rsha_errata = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", rhsa_venom, str(old_object[82]))
                old_object[82] = rsha_errata
                # print(grey_stone, black_stone)
                continue

            if z == 3:
                brews_bros = brew_link + str(jump_man[1])
                build_venom = str(jump_man[1])
                date_venom = str(jump_man[3])
                rhsa_venom = str(jump_man[4])
                url_venom = str(jump_man[5])

                # brew href
                brew_change = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brews_bros, str(old_object[88]))
                old_object[88] = brew_change

                row_new = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", build_venom, str(old_object[89]))
                old_object[89] = row_new

                date_row = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", date_venom, str(old_object[91]))
                old_object[91] = date_row

                # advisory link
                adlink_row = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", url_venom, str(old_object[95]))
                old_object[95] = adlink_row

                # RHSA
                rsha_row = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", rhsa_venom, str(old_object[96]))
                old_object[96] = rsha_row
                continue

        for z in range(0, 2):
            e = Erratum(product='RHEL', errata_id=json_string[2][1][z]['id'])
            jump_man = e.errata_id, e.errata_builds["RHEL-8.1.0.Z.MAIN+EUS"][0], e.errata_state, e.ship_date, e.errata_name, e.url()

            if z == 1:
                brews_bros = brew_link + str(jump_man[1])
                build_venom = str(jump_man[1])
                date_venom = str(jump_man[3])
                rhsa_venom = str(jump_man[4])
                url_venom = str(jump_man[5])

                # grey_ = re.sub(r'8.4.0.z', mile_, str(old_object[100]))
                # old_object[100] = grey_
                #
                # black_ = re.sub(r'batch#\d', stone_, str(old_object[101]))
                # old_object[101] = black_
                # #
                # brew href
                href_name = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brews_bros, str(old_object[105]))
                old_object[105] = href_name

                row_kernel = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", build_venom, str(old_object[106]))
                old_object[106] = row_kernel

                row_date = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", date_venom, str(old_object[108]))
                old_object[108] = row_date

                # # advisory link
                errata_link = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", url_venom, str(old_object[112]))
                old_object[112] = errata_link

                # # RHSA
                row_rsha = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}",rhsa_venom, str(old_object[113]))
                old_object[113] = row_rsha
                continue
        #
        try_it = old_object
        join_table = (' '.join(try_it))
        with open('swap-table-2.html', 'w') as x:
            x.write(str(join_table))
            x.close()

        with open('swap-table-2.html', 'r') as html_file:
            soup = BeautifulSoup(html_file, 'lxml')
            batman = soup.find_all('p', limit=1)
            table_wrap = soup.new_tag("table", style="width:35.4001%")
            new_table = soup.new_tag("table", style="width:35.4001%")
            batman_new = batman

            kstuff = soup.find_all('tr')
            new_array = kstuff
            new_array.copy()

            table_headers = new_array[0]
            release_header = new_array[1]

            first_row = new_array[2]
            second_row = new_array[3]
            third_row = new_array[4]
            fourth_row = new_array[5]
            fifth_row = new_array[6]
            sixth_row = new_array[7]


            table_wrap.insert(1, table_headers)
            table_wrap.insert(2, release_header)
            table_wrap.insert(3, first_row)
            table_wrap.insert(4, second_row)
            table_wrap.insert(5, third_row)
            table_wrap.insert(6, fourth_row)
            table_wrap.insert(7, fifth_row)
            table_wrap.insert(8, sixth_row)


        batman_new.insert(1, table_wrap)
        batman_new.insert(2, new_table)


        q = open('swap-table-2.html', 'w')
        q.write(str(batman_new[0]) + str(batman_new[1]) + str(batman_new[2]))
        q.close()

        with open('swap-table-2.html', 'r') as o:
            my_html = o.read()
            s = my_html
            # print(s)

        auth_token = PAT

        headers = {'Authorization': 'Bearer ' + auth_token,
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'
                   }
        # #
        with open('RHEL8-RT-Release-History-Page-Testing_1.json', 'r') as e:
            json_object = json.loads(e.read())
        num = json_object['version']['number']

        json_string = {
            "version": {
                "number": num +1
            },
            "title": "Release History Page Testing 1",
            "type": "page",
            "space": {"key": "KRATS"},
            "body": {
                "storage": {
                    "value": s,
                    "representation": "storage"
                }
            }
        }

        # put call
        website = 'https://docs.engineering.redhat.com/rest/api/content/304821200?expand=body.view,version'

        post = requests.put(url=website, data=json.dumps(json_string), headers=headers, verify="/etc/ssl/certs/ca-bundle.trust.crt")
        print(post.status_code)

        new_dumps = (json.dumps(json.loads(post.text), sort_keys=True, indent=4, separators=(",", ": ")))
        print(new_dumps)

        z = open('RHEL8-RT-Release-History-Page-Testing_1.json', 'w')

        z.write(str(new_dumps))

        # # # #
        # # # # reassign array values and swap values out https://www.geeksforgeeks.org/change-the-tags-contents-and-replace-with-the-given-string-using-beautifulsoup/
        # # #


if __name__ == '__main__':
    command()

