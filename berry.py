#!/usr/bin/python3.9
import json.decoder
import urllib3
import requests
import json
from API_Token import PAT, brew_link
from bs4 import BeautifulSoup
import re
import NSHS
import NHS

urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")


def command():
        auth_token = PAT

        headers = {'Authorization': 'Bearer ' + auth_token,
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'}

        json_string = {
            "version": {
                "number": +1
            },
            "title": "Test Release History Page",
            "type": "page",
            "space": {"key": "KRATS"},
            "body": {
                "storage": {
                    "value": +1,
                    "representation": "storage"
                }
            }
        }
        website = 'https://docs.engineering.redhat.com/rest/api/content/249904692?expand=body.view,version'

        get = requests.get(url=website, data=json.dumps(json_string), headers=headers, verify="/etc/ssl/certs/ca-bundle.trust.crt")

        new_dumps = (json.dumps(json.loads(get.text), sort_keys=True, indent=4, separators=(",", ": ")))

        # print(new_dumps)

        z = open('RHEL8-RT-Release-History-TEST-PAGE.json', 'w')

        z.write(str(new_dumps))



        with open("RHEL8-RT-Release-History-TEST-PAGE.json", 'r') as e:
            json_object = json.loads(e.read())

        b = json_object['body']['view']['value']
        cool_object = b
        # print(b)

        with open('table-file-1.html', 'w') as lion:
            lion.write(b)
            lion.close()
#
        with open('table-file-1.html', 'w') as x:
            old_object = cool_object.split()
            brew_url = brew_link + NSHS.kernel_version

            # kernel_version, date, rhsa, e_url

            release_header = re.sub(r"\d.\d.\d.\w.\w{1,5}\D\w{1,3}", NSHS.header, str(old_object[16]))
            old_object[16] = release_header
            # print(release_header)



#       Second row
#          # brew href
            updated_brew = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brew_url, str(old_object[41]))
            old_object[41] = updated_brew
            # print(old_object[233],old_object[234],old_object[236],old_object[240],old_object[241])
            #
            # # # nvr
            updated_row = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NSHS.kernel_version, str(old_object[42]))
            old_object[42] = updated_row

             # date
            update_date = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NSHS.date, str(old_object[44]))
            old_object[44] = update_date


            # advisory link
            new_adlink = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NSHS.e_url, str(old_object[48]))
            old_object[48] = new_adlink


            # RHSA
            new_rsha = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NSHS.rhsa, str(old_object[49]))
            old_object[49] = new_rsha
#
#
#         # third row
            brew_href = brew_link + NSHS.g_build

            # brew href
            updated_href = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brew_href, str(old_object[58]))
            old_object[58] = updated_href

            # nvr
            updated_nvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NSHS.g_build, str(old_object[59]))
            old_object[59] = updated_nvr

            # date
            update_shipdate = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NSHS.g_pubdate, str(old_object[61]))
            old_object[61] = update_shipdate

            # advisory link
            ad_link = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NSHS.ratta_link, str(old_object[65]))
            old_object[65] = ad_link

            # RHSA
            next_rhsa = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NSHS.g_rhsa, str(old_object[66]))
            old_object[66] = next_rhsa
#
        # fourth row
            brew_build = brew_link + NSHS.h_build

            # brew href
            updated_build = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brew_build, str(old_object[74]))
            old_object[74] = updated_build
            # print(old_object[74],old_object[75],old_object[77],old_object[81],old_object[82])

            # # nvr
            _nvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NSHS.h_build, str(old_object[75]))
            old_object[75] = _nvr
            # #
            # # # # # # date
            _date = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NSHS.h_pubdate, str(old_object[77]))
            old_object[77] = _date

            # advisory link
            _adlink = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NSHS.h_url, str(old_object[81]))
            old_object[81] = _adlink

            # RHSA
            _rsha = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NSHS.h_rhsa, str(old_object[82]))
            old_object[82] = _rsha

#
        # fifth row
            brew_brew = brew_link + NSHS.i_build

            # brew href
            b_link = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brew_brew, str(old_object[90]))
            old_object[90] = b_link
            # print(old_object[90],old_object[91],old_object[93],old_object[97],old_object[98])

            # nvr
            row_fifth = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NSHS.i_build, str(old_object[91]))
            old_object[91] = row_fifth

            # date
            date_fifth = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NSHS.i_pubdate, str(old_object[93]))
            old_object[93] = date_fifth

            #  advisory link
            adlink_errata = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NSHS.i_url, str(old_object[97]))
            old_object[97] = adlink_errata

            # RHSA
            rsha_errata = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NSHS.i_rhsa, str(old_object[98]))
            old_object[98] = rsha_errata
#
    # 2nd table 1st row

            brew_href = brew_link + NHS.e_nvr
            # brew href
            brew_change = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", brew_href, str(old_object[120]))
            old_object[120] = brew_change

            row_new = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NHS.e_nvr, str(old_object[122]))
            old_object[122] = row_new

            date_row = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NHS.e_date, str(old_object[124]))
            old_object[124] = date_row

            # advisory link
            adlink_row = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+",  NHS.e_brew,  str(old_object[129]))
            old_object[129] = adlink_row

            # RHSA
            rsha_row = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NHS.e_name, str(old_object[129]))
            old_object[129] = rsha_row
            # print(old_object[119],old_object[120],old_object[122],old_object[126],old_object[127])
#
    # 2nd table 2nd row

            href_brew = brew_link + NHS.f_version
            # brew href
            href_name = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", href_brew, str(old_object[136]))
            old_object[136] = href_name
            # print(old_object[136],old_object[137],old_object[139],old_object[143],old_object[144])

            row_kernel = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NHS.f_version, str(old_object[137]))
            old_object[137] = row_kernel

            row_date = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NHS.f_date, str(old_object[139]))
            old_object[139] = row_date

            # # advisory link
            errata_link = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NHS.z_url, str(old_object[143]))
            old_object[143] = errata_link

            # # RHSA
            row_rsha = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NHS.f_rhsa, str(old_object[144]))
            old_object[144] = row_rsha
#
    # 2nd table 3rd row
            href_tag = brew_link + NHS.h_build
            # brew href
            href_href = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", href_tag, str(old_object[153]))
            old_object[153] = href_href
            # print(old_object[153],old_object[154],old_object[156],old_object[160],old_object[161])

            row_nvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NHS.h_build, str(old_object[154]))
            old_object[154] = row_nvr

            date_tag = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NHS.h_pubdate, str(old_object[156]))
            old_object[156] = date_tag

            # # # advisory link
            row_ad = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NHS.h_url, str(old_object[160]))
            old_object[160] = row_ad

            # # RHSA
            row_rsha = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NHS.h_rhsa, str(old_object[161]))
            old_object[161] = row_rsha
#
    # 2nd table 4th row async
            c_tag = brew_link + NHS.g_build

            # brew href
            tag_c = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", c_tag, str(old_object[169]))
            old_object[169] = tag_c
            # print(old_object[169],old_object[170],old_object[172],old_object[176],old_object[177])

            eratta_kvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NHS.g_build, str(old_object[170]))
            old_object[170] = eratta_kvr

            newdate_eratta = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NHS.g_pubdate, str(old_object[172]))
            old_object[172] = newdate_eratta

            # # # advisory link
            eratta_advisory = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+",  NHS.ratta_link, str(old_object[176]))
            old_object[176] = eratta_advisory

            # # RHSA
            ratta_ratta = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NHS.g_rhsa, str(old_object[177]))
            old_object[177] = ratta_ratta
#
     # 2nd table 5th row
            b_tag = brew_link + NHS.i_build

            # brew href
            tag_b = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", b_tag, str(old_object[185]))
            old_object[185] = tag_b
            # print(old_object[185],old_object[186],old_object[188],old_object[192],old_object[193])

            eratta_nvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NHS.i_build, str(old_object[186]))
            old_object[186] = eratta_nvr

            date_eratta = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NHS.i_pubdate, str(old_object[188]))
            old_object[188] = date_eratta

            # # # advisory link
            eratta_ad = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NHS.i_url, str(old_object[192]))
            old_object[192] = eratta_ad

            # # RHSA
            ratta_rsha = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NHS.i_rhsa, str(old_object[193]))
            old_object[193] = ratta_rsha
#
     # 2nd table 6th row
            d_tag = brew_link + NHS.j_build

            # brew href
            tag_d = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", d_tag, str(old_object[201]))
            old_object[201] = tag_d
            # print(old_object[201],old_object[202],old_object[204],old_object[208],old_object[209])

            nvr_kvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NHS.j_build, str(old_object[202]))
            old_object[202] = nvr_kvr

            pubdate_eratta = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NHS.j_pubdate, str(old_object[204]))
            old_object[204] = pubdate_eratta

            # # # advisory link
            era_advisory = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NHS.j_url, str(old_object[208]))
            old_object[208] = era_advisory

            # # RHSA
            my_ratta = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NHS.j_rhsa, str(old_object[209]))
            old_object[209] = my_ratta

    # 2nd table 7th row
            e_tag = brew_link + NHS.k_build

            # brew href
            tag_e = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", e_tag, str(old_object[217]))
            old_object[217] = tag_e
            # print(old_object[217],old_object[218],old_object[220],old_object[224],old_object[225])

            nvid_kvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NHS.k_build, str(old_object[218]))
            old_object[218] = nvid_kvr

            pub_erattadate = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NHS.k_pubdate, str(old_object[220]))
            old_object[220] = pub_erattadate

            # # # advisory link
            my_advisory = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+",NHS.k_url, str(old_object[224]))
            old_object[224] = my_advisory

            # # RHSA
            my_rhsa = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NHS.k_rhsa, str(old_object[225]))
            old_object[225] = my_rhsa

    # 2nd table 8th row
            f_tag = brew_link + NHS.m_build

            # brew href
            tag_f = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", f_tag, str(old_object[233]))
            old_object[233] = tag_f
            # print(old_object[233], old_object[234], old_object[236], old_object[240], old_object[241])


            n_kvr = re.sub(r"kernel\Drt\D\d\.\d{2,4}\.\d\D\d{1,4}\.\d{1,3}\.\d\.\w{1,3}\d{1,3}\.\d{1,4}\.\w{1,4}\d", NHS.m_build, str(old_object[234]))
            old_object[234] = n_kvr

            pub_edate = re.sub(r"\d{1,5}-.\w{1,7}-\d{1,4}", NHS.m_pubdate, str(old_object[236]))
            old_object[236] = pub_edate

            # # # advisory link
            my_visory = re.sub(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", NHS.m_url, str(old_object[240]))
            old_object[240] = my_visory

            # # RHSA
            my_sa = re.sub(r"[a-zA-Z]{1,4}-[0-9]{1,4}:[0-9]{1,4}-[0-9]{1,2}", NHS.m_rhsa, str(old_object[241]))
            old_object[241] = my_sa

            # print(old_object[234],old_object[236] ,old_object[238],old_object[242],old_object[243])
            # print(old_object[174], old_object[178], old_object[179])

            try_it = old_object
            join_table = (' '.join(try_it))
            x.write(join_table)
            x.close()

        with open('table-file-1.html', 'r') as html_file:
            soup = BeautifulSoup(html_file, 'lxml')
            batman = soup.find_all('p', limit=1)
            table_wrap = soup.new_tag("table", style="width:35.4001%")
            new_table = soup.new_tag("table", style="width:35.4001%")
            batman_new = batman

            kstuff = soup.find_all('tr')
            new_array = kstuff
            # new_array.copy()

            table_headers = new_array[0]
            release_header = new_array[1]

            first_row = new_array[2]
            second_row = new_array[3]
            third_row = new_array[4]
            fourth_row = new_array[5]
            fifth_row = new_array[6]

            table_wrap.insert(1, table_headers)
            table_wrap.insert(2, release_header)
            table_wrap.insert(3, first_row)
            table_wrap.insert(4, second_row)
            table_wrap.insert(5, third_row)
            table_wrap.insert(6, fourth_row)
            table_wrap.insert(7, fifth_row)

            headers_table = new_array[7]
            release_title = new_array[8]
            new_row = new_array[9]
            row_second = new_array[10]
            row_third = new_array[11]
            row_fourth = new_array[12]
            row_fifth = new_array[13]
            row_six = new_array[14]
            row_seven = new_array[15]
            row_eight = new_array[16]

            new_table.insert(1, headers_table)
            new_table.insert(2, release_title)
            new_table.insert(3, new_row)
            new_table.insert(4, row_second)
            new_table.insert(5, row_third)
            new_table.insert(6, row_fourth)
            new_table.insert(7, row_fifth)
            new_table.insert(8, row_six)
            new_table.insert(9, row_seven)
            new_table.insert(10, row_eight)
            # print(row_eight)



        batman_new.insert(1, table_wrap)
        batman_new.insert(2, new_table)


        # new_table = batman_new

        # soup = BeautifulSoup(str(new_table[1]), features="lxml")
#
        q = open('table-file-1.html', 'w')
        q.write(str(batman_new[0]) + str(batman_new[1]) + str(batman_new[2]))
        q.close()
#
        with open('table-file-1.html', 'r') as o:
            my_html = o.read()
            s = my_html
            # print(s)
#
# # #
# # # # reassign array values and swap values out https://www.geeksforgeeks.org/change-the-tags-contents-and-replace-with-the-given-string-using-beautifulsoup/
# # #
        auth_token = PAT

        headers = {'Authorization': 'Bearer ' + auth_token,
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'
                   }
        #
        with open('RHEL8-RT-Release-History-TEST-PAGE.json', 'r') as e:
            json_object = json.loads(e.read())
        num = json_object['version']['number']

        json_string = {
            "version": {
                "number": num +1
            },
            "title": "Test Release History Page",
            "type": "page",
            "space": {"key": "KRATS"},
            "body": {
                "storage": {
                    "value": s,
                    "representation": "storage"
                }
            }
                        }

        # put call
        website = 'https://docs.engineering.redhat.com/rest/api/content/249904692?expand=body.view,space'

        post = requests.put(url=website, data=json.dumps(json_string), headers=headers, verify="/etc/ssl/certs/ca-bundle.trust.crt")

        new_dumps = (json.dumps(json.loads(post.text), sort_keys=True, indent=4, separators=(",", ": ")))

        print(new_dumps)

        z = open('RHEL8-RT-Release-History-TEST-PAGE.json', 'w')

        z.write(str(new_dumps))
#
#
if __name__ == '__main__':
    command()
