﻿import urllib3
import json
import os
from errata_tool import Erratum
from errata_tool.release import Release

urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs="/etc/ssl/certs/ca-bundle.trust.crt")

if 'REQUESTS_CA_BUNDLE' not in os.environ:

    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/pki/tls/certs/ca-bundle.crt'


relea = Release(name='RHEL-8.4.0.Z.EUS')

advisories = relea.advisories()
rhel_prep = [a for a in advisories if a['status'] == 'REL_PREP']

json_son = json.dumps(rhel_prep, indent=2, sort_keys=True)

print(json_son)
#


rele = Release(name='RHEL-8.2.0.Z.EUS')

advisories = rele.advisories()
ship_live = [a for a in advisories if a['status'] == 'SHIPPED_LIVE']

json_data = json.dumps(ship_live, indent=4, sort_keys=True)


print(json_data)
#
#
release = Release(name='RHEL-8.4.0.Z.EUS')

advisories = release.advisories()
release_prep = [a for a in advisories if a['status'] == 'REL_PREP']

son_json = json.dumps(release_prep, indent=4, sort_keys=True)

print(son_json)


#
#
releases = Release(name='RHEL-8.4.0.Z.EUS')

advisories = releases.advisories()
rel_prep = [a for a in advisories if a['status'] == 'SHIPPED_LIVE']

json_string = json.dumps(rel_prep, indent=4, sort_keys=True)

# with open('RHEL-EUS.json', 'w') as t:
#     t.write(str(json_string))
#     t.close()
print(json_string)
#
# with open('RHEL-EUS.json', 'r') as e:
#     json_object = json.loads(e.read())
#
# b = json_object["advisories"]
#
# print(b)


print(rel_prep)  # prints the list of advisories' data

ga_rel = Release(name='RHEL-8.4.0.GA')

advisories = ga_rel.advisories()
ga_files = [a for a in advisories if a['status'] == 'SHIPPED_LIVE']
# ga_files = [c for c in advisories if c['synopsis'] == 'Important: kernel-rt security and bug fix update']
json_son = json.dumps(ga_files, indent=2, sort_keys=True)
print(json_son)



print(ga_files)  # prints the list of advisories' data

main_rel = Release(name='RHEL-8.4.0.Z.MAIN+EUS')

advisories = main_rel.advisories()
main_files = [a for a in advisories if a['qe_group'] == 'Kernel General QE']
json_jason = json.dumps(main_files, indent=2, sort_keys=True)
print(json_jason)



id =input("errata id?: ")

e = Erratum(product='RHEL', errata_id=id)


print(e.errata_state)
print(e.url())
print(e.errata_name)
print(e.errata_builds, e.publish_date)
my_json = e.get_erratum_data()

json_my = json.dumps(my_json, indent=2)

print(json_my)

